# poc k8s mitmproxy

Use a [mitmproxy](https://mitmproxy.org/) to view http traffic between your ingress-nginx controller and your target Service.

By default, it uses `mitmweb`, but you can change the command in `deployment.yaml` to use `mitmdump`.

## Requirements

The following up and running components:

* k8s cluster
* ingress-nginx ingress controller
* cert-manager and a cluster-issuer named `letsencrypt`
* your service + deployment + pod
* kubectl

## Before deploying

Replace the following strings by real values in the manifests below.

In `deployment.yaml`,

* `APP_SERVICE_NAME`
* `APP_SERVICE_NAMESPACE`
* `APP_SERVICE_PORT`

In `ingress.yaml`,

* `MY_PUBLIC_IP`, allows only this IP to access the Ingress (this IP will be configured in an allowlist by nginx)
* `MY_PUBLIC_HOSTNAME`, for example mitmproxy.example.com. That url needs to be reachable from the Internet

## Deployment

With kubectl,

check that manifests are correct:

```
kubectl apply -f *.yaml --dry-run=client
```

With kubectl,

deploy the manifests:

```
kubectl apply -f namespace.yaml
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
kubectl apply -f ingress.yaml
```

## After deployment

### Web UI

Configure a port forward to mitmproxy web UI:

```
kubectl -nmitmproxy port-forward deployment/mitmproxy-deployment 8081:8081
```

In tour browser, open `localhost:8081`.

### Debug HTTP traffic

Now you can make http requests to your ingress URL.

In Web UI, you'll see the HTTP request made by mitmproxy to APP_SERVICE_NAME, and it's corresponding response.
